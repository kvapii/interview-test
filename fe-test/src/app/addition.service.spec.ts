/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AdditionService } from './addition.service';

describe('Service: Addition', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdditionService]
    });
  });

  it('should ...', inject([AdditionService], (service: AdditionService) => {
    expect(service).toBeTruthy();
  }));
});
