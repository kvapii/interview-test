import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Params } from 'src/app/params';

@Injectable({
  providedIn: 'root'
})
export class AdditionService {

constructor(private http: HttpClient) { }

addition(params: Params): Observable<any> {
  return this.http.put('http://localhost:8085/addition', params);
}

}
