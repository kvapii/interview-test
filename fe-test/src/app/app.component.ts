import { LogService } from './log.service';
import { AdditionService } from './addition.service';
import { Params } from './params';
import { Component, OnInit } from '@angular/core';
import { interval, timer } from 'rxjs';
import { delay } from 'q';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  constructor(private additionService: AdditionService,
              private logService: LogService) {}

  title = 'fe-test';

  params = new Params();
  result = '';
  logs: any;

  ngOnInit(): void {
    this.getLogs();
  }

  addition() {
    this.additionService.addition(this.params).subscribe(result => this.result = result);
  }

  getLogs() {
    timer(0, 30000)
  .subscribe(() => this.logService.getLog().subscribe(logs => this.logs = logs));

  }
}
