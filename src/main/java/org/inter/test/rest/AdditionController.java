package org.inter.test.rest;

import org.inter.test.domain.Params;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AdditionController {

    @PutMapping("/addition")
    public ResponseEntity<Long> addition(@Valid @RequestBody Params params){
        final Long result = new Long(params.getParam1()) + new Long(params.getParam2());
        return ResponseEntity.ok(result);
    }
}
