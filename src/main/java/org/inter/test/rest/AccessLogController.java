package org.inter.test.rest;

import org.ehcache.Cache;
import org.inter.test.domain.LogMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class AccessLogController {

    @Autowired
    private Cache<String, LogMessage> accessCache;

    @GetMapping("/log")
    public List<LogMessage> accessLog(){
        final List<LogMessage> log = new ArrayList<>();
        accessCache.forEach(it -> log.add(it.getValue()));
        return log.stream().filter(it -> "/addition".equals(it.getUri()))
                .sorted(Comparator.comparing(LogMessage::getCreatedDate).reversed())
                .collect(Collectors.toList());
    }
}
