package org.inter.test.common.logging;

import org.ehcache.Cache;
import org.inter.test.domain.LogMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.http.HttpProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

@Component
public class RequestLogging extends OncePerRequestFilter {

    @Autowired
    private Cache<String, LogMessage> accessCache;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {

        final ContentCachingRequestWrapper requestWrapper = new ContentCachingRequestWrapper(request);
        final ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(response);

        filterChain.doFilter(requestWrapper, responseWrapper);

        final String body = new String(requestWrapper.getContentAsByteArray());
        final String formatedBody = body.replaceAll("\\r|\\n|\\s","");
        responseWrapper.copyBodyToResponse();
        final String uuid = UUID.randomUUID().toString();

        if(!RequestMethod.OPTIONS.name().equals(request.getMethod())) {
            accessCache.put(uuid,
                    new LogMessage(uuid, request.getRemoteAddr(), request.getRequestURI(), formatedBody, response.getStatus(), LocalDateTime.now()));
        }

    }

}
