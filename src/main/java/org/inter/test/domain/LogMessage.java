package org.inter.test.domain;

import java.time.LocalDateTime;

public class LogMessage {

    private String uuid;
    private String ipAddress;
    private String uri;
    private String parameters;
    private int httpStatus;
    private LocalDateTime createdDate;

    public LogMessage() {
    }

    public LogMessage(String uuid, String ipAddress, String uri, String parameters, int httpStatus, LocalDateTime createdDate) {
        this.uuid = uuid;
        this.ipAddress = ipAddress;
        this.uri = uri;
        this.parameters = parameters;
        this.httpStatus = httpStatus;
        this.createdDate = createdDate;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }
}
