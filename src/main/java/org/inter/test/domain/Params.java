package org.inter.test.domain;


import javax.validation.constraints.NotNull;

public class Params {
    @NotNull
    private Integer param1;
    @NotNull
    private Integer param2;

    public Integer getParam1() {
        return param1;
    }

    public void setParam1(Integer param1) {
        this.param1 = param1;
    }

    public Integer getParam2() {
        return param2;
    }

    public void setParam2(Integer param2) {
        this.param2 = param2;
    }
}
